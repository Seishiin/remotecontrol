using CommandLine;

// ReSharper disable ClassNeverInstantiated.Global

namespace RemoteControlServer
{
	internal class Options
	{
		[Option('p', "port", Required = false, Default = 9999, HelpText = "The server will run on this port.")]
		public int Port { get; set; }

		[Option('g', "generate", Required = false, HelpText = "Generates a file containing all available keycodes.")]
		public bool GenerateKeyCodes { get; set; }
	}
}