using System;
using System.Runtime.InteropServices;

// ReSharper disable SuspiciousTypeConversion.Global

namespace RemoteControlServer
{
	public static class VolumeHelper
	{
		public static void SetMasterMute(bool mute)
		{
			var deviceEnumerator = (IMMDeviceEnumerator) new MMDeviceEnumerator();
			deviceEnumerator.GetDefaultAudioEndpoint(EDataFlow.eRender, ERole.eMultimedia, out IMMDevice speakers);

			Guid iid = typeof(IAudioEndpointVolume).GUID;
			speakers.Activate(ref iid, 0, IntPtr.Zero, out object o);

			if (!(o is IAudioEndpointVolume audioEndpointVolume)) return;

			audioEndpointVolume.SetMute(mute, iid);
			Marshal.ReleaseComObject(o);
		}
	}

	[ComImport]
	[Guid("BCDE0395-E52F-467C-8E3D-C4579291692E")]
	internal class MMDeviceEnumerator { }

	internal enum EDataFlow
	{
		eRender
	}

	internal enum ERole
	{
		eMultimedia
	}

	[Guid("A95664D2-9614-4F35-A746-DE8DB63617E6"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	internal interface IMMDeviceEnumerator
	{
		int NotImpl0();

		[PreserveSig]
		int GetDefaultAudioEndpoint(EDataFlow dataFlow, ERole role, out IMMDevice ppDevice);
	}

	[Guid("D666063F-1587-4E43-81F1-B948E807363F"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	internal interface IMMDevice
	{
		[PreserveSig]
		int Activate(ref Guid iid, int dwClsCtx, IntPtr pActivationParams, [MarshalAs(UnmanagedType.IUnknown)] out object ppInterface);
	}

	[Guid("5CDF2C82-841E-4546-9722-0CF74078229A"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	internal interface IAudioEndpointVolume
	{
		[PreserveSig]
		int NotImpl0();

		[PreserveSig]
		int NotImpl1();

		[PreserveSig]
		int NotImpl2();

		[PreserveSig]
		int NotImpl3();

		[PreserveSig]
		int NotImpl4();

		[PreserveSig]
		int NotImpl5();

		[PreserveSig]
		int NotImpl6();

		[PreserveSig]
		int NotImpl7();

		[PreserveSig]
		int NotImpl8();

		[PreserveSig]
		int NotImpl9();

		[PreserveSig]
		int NotImpl10();

		[PreserveSig]
		int SetMute([In] [MarshalAs(UnmanagedType.Bool)] bool bMute, [In] [MarshalAs(UnmanagedType.LPStruct)]
			Guid eventContext);
	}
}