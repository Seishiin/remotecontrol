﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using WindowsInput;
using WindowsInput.Native;
using CommandLine;

namespace RemoteControlServer
{
	public static class Program
	{
		private static readonly InputSimulator InputSimulator = new InputSimulator();
		private static readonly VirtualKeyCode[] KeyCodes = Enum.GetValues(typeof(VirtualKeyCode)).Cast<VirtualKeyCode>().ToArray();
		private static readonly Regex MotionRegex = new Regex(@"MOTION_-?\d*_-?\d*", RegexOptions.ECMAScript);

		public static void Main(string[] args)
		{
			int port = 9999;
			bool generateKeyCodes = false;

			Parser.Default.ParseArguments<Options>(args)
			      .WithParsed(options =>
			       {
				       port = options.Port;
				       generateKeyCodes = options.GenerateKeyCodes;
			       })
			      .WithNotParsed(errors => { Environment.Exit(0); });

			if (generateKeyCodes)
				File.WriteAllLines("keycodes.txt", KeyCodes.Select(keyCode => keyCode.ToString()));

			TcpListener listener = null;
			Socket socket = null;

			Console.WriteLine($"The IP address of the current device is: {Util.GetLocalIpAddress()}");
			
			try
			{
				listener = new TcpListener(IPAddress.Any, port);
				listener.Start();

				Console.WriteLine($"The server is running on port {port}");

				while (true)
				{
					Console.WriteLine("Waiting for a connection...");
					socket = listener.AcceptSocket();
					Console.WriteLine("Connection accepted from " + socket.RemoteEndPoint);

					while (IsSocketConnected(socket))
					{
						byte[] buffer = new byte[256];
						int byteCount = socket.Receive(buffer);

						string msg = Encoding.UTF8.GetString(buffer, 0, byteCount);

						if (!string.IsNullOrWhiteSpace(msg))
							ProcessMessage(msg);
					}

					socket.Close();
					Console.WriteLine("Connection closed");
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
			}
			finally
			{
				socket?.Close();
				listener?.Server.Close();
			}
		}

		private static bool IsSocketConnected(Socket s)
		{
			return !(s.Poll(1000, SelectMode.SelectRead) && s.Available == 0 || !s.Connected);
		}

		private static void ProcessMessage(string msg)
		{
			if (PerformMoveMouse(msg)) return;
			if (PerformCharEntry(msg)) return;
			if (PerformTextEntry(msg)) return;
			if (PerformKeyPress(msg)) return;
			if (PerformCommand(msg)) return;

			Console.WriteLine($"{Util.TimeInfoPrefix()} RECEIVED UNKNOWN MESSAGE: {msg}");
		}

		private static bool PerformMoveMouse(string msg)
		{
			string[] matches = MotionRegex
			                  .Matches(msg)
			                  .OfType<Match>()
			                  .Select(m => m.Groups[0].Value)
			                  .ToArray();

			if (matches.Length == 0) return false;

			foreach (string match in matches)
			{
				string[] data = match.Split('_');
				int deltaX = Convert.ToInt32(data[1]);
				int deltaY = Convert.ToInt32(data[2]);

				Console.WriteLine($"{Util.TimeInfoPrefix()} Performing MoveMouse({deltaX}, {deltaY})");
				InputSimulator.Mouse.MoveMouseBy(deltaX, deltaY);
			}

			return true;
		}

		private static bool PerformTextEntry(string msg)
		{
			if (msg.Length >= 3
			    && msg.First() == Constants.TEXT_ENTRY_SEPARATOR
			    && msg.Last() == Constants.TEXT_ENTRY_SEPARATOR)
			{
				Console.WriteLine($"{Util.TimeInfoPrefix()} Performing TextEntry({msg})");
				InputSimulator.Keyboard.TextEntry(msg.Trim(Constants.TEXT_ENTRY_SEPARATOR));
				return true;
			}

			return false;
		}

		private static bool PerformCharEntry(string msg)
		{
			if (msg.Length == 3
			    && msg.First() == Constants.CHAR_ENTRY_SEPARATOR
			    && msg.Last() == Constants.CHAR_ENTRY_SEPARATOR)
			{
				Console.WriteLine($"{Util.TimeInfoPrefix()} Performing CharEntry({msg})");
				InputSimulator.Keyboard.TextEntry(msg[1]);
				return true;
			}

			return false;
		}

		private static bool PerformKeyPress(string msg)
		{
			switch (msg)
			{
				case Constants.LEFT_MOUSE_BUTTON:
					Console.WriteLine($"{Util.TimeInfoPrefix()} Performing LeftButtonClick");
					InputSimulator.Mouse.LeftButtonClick();
					return true;
				case Constants.RIGHT_MOUSE_BUTTON:
					Console.WriteLine($"{Util.TimeInfoPrefix()} Performing RightButtonClick");
					InputSimulator.Mouse.RightButtonClick();
					return true;
				case Constants.MIDDLE_MOUSE_BUTTON:
					Console.WriteLine($"{Util.TimeInfoPrefix()} Performing MiddleButtonClick");
					InputSimulator.Mouse.MiddleButtonClick();
					return true;
			}

			foreach (var keyCode in KeyCodes)
			{
				if (msg == keyCode.ToString())
				{
					Console.WriteLine($"{Util.TimeInfoPrefix()} Performing KeyPress({keyCode})");
					InputSimulator.Keyboard.KeyPress(keyCode);
					return true;
				}
			}

			return false;
		}

		private static bool PerformCommand(string msg)
		{
			switch (msg)
			{
				case Constants.COMMAND_SHUTDOWN:
					Console.WriteLine($"{Util.TimeInfoPrefix()} Performing Command({msg})");
					Util.Shutdown();
					return true;
				case Constants.COMMAND_REBOOT:
					Console.WriteLine($"{Util.TimeInfoPrefix()} Performing Command({msg})");
					Util.Reboot();
					return true;
				case Constants.COMMAND_TASK_MANAGER:
					Console.WriteLine($"{Util.TimeInfoPrefix()} Performing Command({msg})");
					Util.StartTaskManager();
					return true;
				case Constants.COMMAND_DEVICE_MANAGER:
					Console.WriteLine($"{Util.TimeInfoPrefix()} Performing Command({msg})");
					Util.StartDeviceManager();
					return true;
				case Constants.COMMAND_SYSTEM_INFORMATION:
					Console.WriteLine($"{Util.TimeInfoPrefix()} Performing Command({msg})");
					Util.ShowSystemInformation();
					return true;
				case Constants.COMMAND_SYSTEM_PROPERTIES:
					Console.WriteLine($"{Util.TimeInfoPrefix()} Performing Command({msg})");
					Util.ShowSystemProperties();
					return true;
				case Constants.COMMAND_MUTE:
					Console.WriteLine($"{Util.TimeInfoPrefix()} Performing Command({msg})");
					VolumeHelper.SetMasterMute(true);
					return true;
				case Constants.COMMAND_UNMUTE:
					Console.WriteLine($"{Util.TimeInfoPrefix()} Performing Command({msg})");
					VolumeHelper.SetMasterMute(false);
					return true;
			}

			return false;
		}
	}
}