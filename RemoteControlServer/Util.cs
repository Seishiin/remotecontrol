﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;

namespace RemoteControlServer
{
	public static class Util
	{
		public static void Shutdown()
		{
			Process.Start(new ProcessStartInfo("shutdown.exe", "/s /t 0")
			{
				CreateNoWindow = true,
				UseShellExecute = false
			});
		}

		public static void Reboot()
		{
			Process.Start(new ProcessStartInfo("shutdown.exe", "/r /t 0")
			{
				CreateNoWindow = true,
				UseShellExecute = false
			});
		}

		public static void StartTaskManager()
		{
			Process.Start(new ProcessStartInfo("taskmgr.exe"));
		}

		public static void StartDeviceManager()
		{
			Process.Start(new ProcessStartInfo("devmgmt.msc"));
		}

		public static void ShowSystemInformation()
		{
			Process.Start(new ProcessStartInfo("msinfo32.exe"));
		}

		public static void ShowSystemProperties()
		{
			Process.Start(new ProcessStartInfo("Sysdm.cpl"));
		}

		public static string TimeInfoPrefix()
		{
			return $"[{DateTime.Now:yyyy/MM/dd HH:mm:ss}]";
		}

		public static string GetLocalIpAddress()
		{
			UnicastIPAddressInformation mostSuitableIp = null;
			NetworkInterface[] networkInterfaces = NetworkInterface.GetAllNetworkInterfaces();

			foreach (var network in networkInterfaces)
			{
				if (network.OperationalStatus != OperationalStatus.Up)
					continue;

				IPInterfaceProperties properties = network.GetIPProperties();

				if (properties.GatewayAddresses.Count == 0)
					continue;

				foreach (var address in properties.UnicastAddresses)
				{
					if (address.Address.AddressFamily != AddressFamily.InterNetwork)
						continue;

					if (IPAddress.IsLoopback(address.Address))
						continue;

					if (!address.IsDnsEligible)
					{
						mostSuitableIp ??= address;
						continue;
					}

					if (address.PrefixOrigin != PrefixOrigin.Dhcp)
					{
						if (mostSuitableIp == null || !mostSuitableIp.IsDnsEligible)
							mostSuitableIp = address;

						continue;
					}

					return address.Address.ToString();
				}
			}

			return mostSuitableIp != null ? mostSuitableIp.Address.ToString() : "";
		}
	}
}