﻿// ReSharper disable InconsistentNaming

namespace RemoteControlServer
{
	public static class Constants
	{
		public const string LEFT_MOUSE_BUTTON = "LBUTTON";
		public const string RIGHT_MOUSE_BUTTON = "RBUTTON";
		public const string MIDDLE_MOUSE_BUTTON = "MBUTTON";

		public const string COMMAND_SHUTDOWN = "CMD_SHUTDOWN";
		public const string COMMAND_REBOOT = "CMD_REBOOT";
		public const string COMMAND_TASK_MANAGER = "CMD_TASKMGR";
		public const string COMMAND_DEVICE_MANAGER = "CMD_DEVICEMGR";
		public const string COMMAND_SYSTEM_INFORMATION = "CMD_SYSINFO";
		public const string COMMAND_SYSTEM_PROPERTIES = "CMD_SYSPROP";
		public const string COMMAND_MUTE = "CMD_MUTE";
		public const string COMMAND_UNMUTE = "CMD_UNMUTE";

		public const char TEXT_ENTRY_SEPARATOR = '"';
		public const char CHAR_ENTRY_SEPARATOR = '\'';
	}
}