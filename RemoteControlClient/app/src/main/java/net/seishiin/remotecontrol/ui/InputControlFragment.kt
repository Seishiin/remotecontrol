package net.seishiin.remotecontrol.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import net.seishiin.remotecontrol.R
import net.seishiin.remotecontrol.data.Constants
import net.seishiin.remotecontrol.data.TCP
import net.seishiin.remotecontrol.databinding.FragmentInputControlBinding
import net.seishiin.remotecontrol.vm.StatusViewModel

class InputControlFragment : Fragment() {

	private val viewModel by activityViewModels<StatusViewModel>()

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
		return inflater.inflate(R.layout.fragment_input_control, container, false)
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)
		val binding = FragmentInputControlBinding.bind(view)

		initVM(binding)
		initUI(binding)
	}

	private fun initVM(b: FragmentInputControlBinding) {
		viewModel.connectionStatus.observe(viewLifecycleOwner) { text -> b.inputControlTextViewStatus.text = text }
	}

	private fun initUI(b: FragmentInputControlBinding) {
		b.inputControlButtonShutdown.setOnClickListener {
			sendMessage(Constants.COMMAND_SHUTDOWN)
		}

		b.inputControlButtonReboot.setOnClickListener {
			sendMessage(Constants.COMMAND_REBOOT)
		}

		b.inputControlButtonTaskManager.setOnClickListener {
			sendMessage(Constants.COMMAND_TASK_MANAGER)
		}

		b.inputControlButtonDeviceManager.setOnClickListener {
			sendMessage(Constants.COMMAND_DEVICE_MANAGER)
		}

		b.inputControlButtonSystemInfo.setOnClickListener {
			sendMessage(Constants.COMMAND_SYSTEM_INFORMATION)
		}

		b.inputControlButtonSystemProp.setOnClickListener {
			sendMessage(Constants.COMMAND_SYSTEM_PROPERTIES)
		}

		b.inputControlButtonMute.setOnClickListener {
			sendMessage(Constants.COMMAND_MUTE)
		}

		b.inputControlButtonUmute.setOnClickListener {
			sendMessage(Constants.COMMAND_UNMUTE)
		}
	}

	private fun sendMessage(msg: String?) {
		lifecycleScope.launch(Dispatchers.IO) {
			viewModel.updateStatus()
			try {
				TCP.sendMsg(msg)
			} catch (e: Exception) {
				TCP.reconnect()
			}
		}
	}
}