package net.seishiin.remotecontrol.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.fragment.app.Fragment
import net.seishiin.remotecontrol.R
import net.seishiin.remotecontrol.data.AppPreferences
import net.seishiin.remotecontrol.databinding.FragmentSettingsBinding
import net.seishiin.remotecontrol.helper.Util

class SettingsFragment : Fragment() {

	private val isValidIpv4 = Regex("^((25[0-5]|(2[0-4]|1[0-9]|[1-9]|)[0-9])(\\.(?!\$)|\$)){4}\$")

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
		return inflater.inflate(R.layout.fragment_settings, container, false)
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)
		val binding = FragmentSettingsBinding.bind(view)

		loadPrefs(binding)
		initUI(binding)
	}

	private fun loadPrefs(b: FragmentSettingsBinding) {
		b.settingsEditTextIpAddress.setText(AppPreferences.ip)
		b.settingsEditTextPort.setText(AppPreferences.port.toString())
		b.settingsEditTextMessage.setText(AppPreferences.message)
		b.settingsEditTextMessage2.setText(AppPreferences.message2)
	}

	private fun initUI(b: FragmentSettingsBinding) {
		b.settingsEditTextIpAddress.setOnEditorActionListener { view, actionId, _ ->
			if (actionId == EditorInfo.IME_ACTION_DONE) {
				val ip = b.settingsEditTextIpAddress.text.toString()
				Util.hideKeyboard(view)
				if (isValidIpv4.matches(ip))
					AppPreferences.ip = ip
				else
					b.settingsEditTextIpAddress.setText(AppPreferences.ip)
				true
			}
			else
				false
		}

		b.settingsEditTextPort.setOnEditorActionListener { view, actionId, _ ->
			if (actionId == EditorInfo.IME_ACTION_DONE) {
				val port = b.settingsEditTextPort.text.toString().toInt()
				Util.hideKeyboard(view)
				if (port in 0..65535)
					AppPreferences.port = port
				else {
					println(AppPreferences.port)
					b.settingsEditTextPort.setText(AppPreferences.port.toString())
				}
				true
			}
			else
				false
		}

		b.settingsEditTextMessage.setOnEditorActionListener { view, actionId, _ ->
			if (actionId == EditorInfo.IME_ACTION_DONE) {
				val msg = b.settingsEditTextMessage.text.toString()
				Util.hideKeyboard(view)
				AppPreferences.message = msg
				true
			}
			else
				false
		}

		b.settingsEditTextMessage2.setOnEditorActionListener { view, actionId, _ ->
			if (actionId == EditorInfo.IME_ACTION_DONE) {
				val msg = b.settingsEditTextMessage2.text.toString()
				Util.hideKeyboard(view)
				AppPreferences.message2 = msg
				true
			}
			else
				false
		}
	}
}