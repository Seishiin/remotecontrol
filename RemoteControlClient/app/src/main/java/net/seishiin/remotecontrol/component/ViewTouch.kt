package net.seishiin.remotecontrol.component

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import kotlin.math.roundToInt

class ViewTouch(context: Context, attrs: AttributeSet) : View(context, attrs) {

	private var prevX = 0
	private var prevY = 0
	private var startTime: Long = 0
	private val maxDelay = 200

	var onDoubleTouch: ((Unit) -> Unit)? = null
	var onMove: ((Int, Int) -> Unit)? = null

	@SuppressLint("ClickableViewAccessibility")
	override fun onTouchEvent(event: MotionEvent?): Boolean {
		if (event != null) {
			val currentX = event.rawX.roundToInt()
			val currentY = event.rawY.roundToInt()

			when (event.action) {

				MotionEvent.ACTION_UP -> {
					startTime = System.currentTimeMillis()
				}

				MotionEvent.ACTION_DOWN -> {
					if (System.currentTimeMillis() - startTime <= maxDelay)
						onDoubleTouch?.invoke(Unit)

					prevX = currentX
					prevY = currentY

					return true
				}

				MotionEvent.ACTION_MOVE -> {
					onMove?.invoke(currentX - prevX, currentY - prevY)

					prevX = currentX
					prevY = currentY
				}
			}
		}

		return super.onTouchEvent(event)
	}
}