package net.seishiin.remotecontrol.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import net.seishiin.remotecontrol.R
import net.seishiin.remotecontrol.data.AppPreferences
import net.seishiin.remotecontrol.data.TCP
import net.seishiin.remotecontrol.databinding.FragmentInputBasicBinding
import net.seishiin.remotecontrol.vm.StatusViewModel

class InputBasicFragment : Fragment() {

	private val viewModel by activityViewModels<StatusViewModel>()

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
		return inflater.inflate(R.layout.fragment_input_basic, container, false)
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)
		val binding = FragmentInputBasicBinding.bind(view)

		initVM(binding)
		initUI(binding)
	}

	private fun initVM(b: FragmentInputBasicBinding) {
		viewModel.connectionStatus.observe(viewLifecycleOwner) { text -> b.inputBasicTextViewStatus.text = text }
		viewModel.buttonText.observe(viewLifecycleOwner) { text -> b.inputBasicButtonSend.text = text }
		viewModel.button2Text.observe(viewLifecycleOwner) { text -> b.inputBasicButtonSend2.text = text }

		viewModel.updateButtons()
	}

	private fun initUI(b: FragmentInputBasicBinding) {
		b.inputBasicButtonSend.setOnClickListener {
			sendMessage(AppPreferences.message)
		}

		b.inputBasicButtonSend2.setOnClickListener {
			sendMessage(AppPreferences.message2)
		}
	}

	private fun sendMessage(msg: String?) {
		lifecycleScope.launch(Dispatchers.IO) {
			viewModel.updateStatus()
			try {
				TCP.sendMsg(msg)
			} catch (e: Exception) {
				TCP.reconnect()
			}
		}
	}
}