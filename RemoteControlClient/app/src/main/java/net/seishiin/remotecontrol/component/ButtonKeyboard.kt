package net.seishiin.remotecontrol.component

import android.annotation.SuppressLint
import android.content.Context
import android.text.InputType
import android.util.AttributeSet
import android.view.KeyEvent
import android.view.MotionEvent
import android.view.inputmethod.BaseInputConnection
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputConnection
import androidx.appcompat.widget.AppCompatButton
import net.seishiin.remotecontrol.helper.Util

class ButtonKeyboard(context: Context, attrs: AttributeSet) : AppCompatButton(context, attrs) {

	var onKeyPressed: ((String) -> Unit)? = null

	@SuppressLint("ClickableViewAccessibility")
	override fun onTouchEvent(event: MotionEvent): Boolean {
		super.onTouchEvent(event)
		if (event.action == MotionEvent.ACTION_UP)
			Util.showKeyboard(this)

		return true
	}

	override fun onCreateInputConnection(outAttrs: EditorInfo): InputConnection {
		val connection = BaseInputConnection(this, false)
		outAttrs.actionLabel = null
		outAttrs.inputType = InputType.TYPE_NULL
		outAttrs.imeOptions = EditorInfo.IME_ACTION_GO
		return connection
	}

	override fun onCheckIsTextEditor(): Boolean {
		return true
	}

	override fun dispatchKeyEvent(event: KeyEvent): Boolean {
		if (event.keyCode == 66) {
			if (event.action == KeyEvent.ACTION_DOWN)
				onKeyPressed?.invoke(Util.convertKey(event))
			return true
		}

		if (event.action == KeyEvent.ACTION_DOWN) {
			onKeyPressed?.invoke(Util.convertKey(event))
			return true
		}

		return false
	}

	init {
		isFocusableInTouchMode = true
	}
}