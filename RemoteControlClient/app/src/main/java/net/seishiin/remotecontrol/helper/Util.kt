package net.seishiin.remotecontrol.helper

import android.content.Context
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView

object Util {

	fun hideKeyboard(tv: TextView) {
		val imm: InputMethodManager = tv.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
		imm.hideSoftInputFromWindow(tv.windowToken, 0)
	}

	fun showKeyboard(v: View) {
		val imm: InputMethodManager = v.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
		imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
	}

	fun convertKey(event: KeyEvent): String {
		if (event.unicodeChar == 0 || event.keyCode == 66)
			return keyCodes.find { it.first == event.keyCode }?.second ?: ""

		return unicodeChars.find { it.first == event.unicodeChar }?.second ?: ""
	}

	private val keyCodes: List<Pair<Int, String>> = listOf(
			Pair(59, "LSHIFT"),
			Pair(66, "RETURN"),
			Pair(67, "BACK")
	)

	private val unicodeChars: List<Pair<Int, String>> = listOf(
			Pair(48, "VK_0"),
			Pair(49, "VK_1"),
			Pair(50, "VK_2"),
			Pair(51, "VK_3"),
			Pair(52, "VK_4"),
			Pair(53, "VK_5"),
			Pair(54, "VK_6"),
			Pair(55, "VK_7"),
			Pair(56, "VK_8"),
			Pair(57, "VK_9"),

			Pair(65, "'A'"),
			Pair(66, "'B'"),
			Pair(67, "'C'"),
			Pair(68, "'D'"),
			Pair(69, "'E'"),
			Pair(70, "'F'"),
			Pair(71, "'G'"),
			Pair(72, "'H'"),
			Pair(73, "'I'"),
			Pair(74, "'J'"),
			Pair(75, "'K'"),
			Pair(76, "'L'"),
			Pair(77, "'M'"),
			Pair(78, "'N'"),
			Pair(79, "'O'"),
			Pair(80, "'P'"),
			Pair(81, "'Q'"),
			Pair(82, "'R'"),
			Pair(83, "'S'"),
			Pair(84, "'T'"),
			Pair(85, "'U'"),
			Pair(86, "'V'"),
			Pair(87, "'W'"),
			Pair(88, "'X'"),
			Pair(89, "'Y'"),
			Pair(90, "'Z'"),

			Pair(97, "VK_A"),
			Pair(98, "VK_B"),
			Pair(99, "VK_C"),
			Pair(100, "VK_D"),
			Pair(101, "VK_E"),
			Pair(102, "VK_F"),
			Pair(103, "VK_G"),
			Pair(104, "VK_H"),
			Pair(105, "VK_I"),
			Pair(106, "VK_J"),
			Pair(107, "VK_K"),
			Pair(108, "VK_L"),
			Pair(109, "VK_M"),
			Pair(110, "VK_N"),
			Pair(111, "VK_O"),
			Pair(112, "VK_P"),
			Pair(113, "VK_Q"),
			Pair(114, "VK_R"),
			Pair(115, "VK_S"),
			Pair(116, "VK_T"),
			Pair(117, "VK_U"),
			Pair(118, "VK_V"),
			Pair(119, "VK_W"),
			Pair(120, "VK_X"),
			Pair(121, "VK_Y"),
			Pair(122, "VK_Z"),

			Pair(42, "MULTIPLY"),
			Pair(43, "ADD"),
			Pair(45, "SUBTRACT"),
			Pair(47, "DIVIDE"),

			Pair(32, "SPACE"),

			Pair(223, "'ß'"),

			Pair(46, "'.'"),
			Pair(44, "','"),

			Pair(33, "'!'"),
			Pair(64, "'@'"),
			Pair(35, "'#'"),
			Pair(36, "'$'"),
			Pair(37, "'%'"),
			Pair(94, "'^'"),
			Pair(38, "'&'"),
			Pair(42, "'*'"),
			Pair(40, "'('"),
			Pair(41, "')'"),

			Pair(95, "'_'"),
			Pair(34, "'\"'"),
			Pair(39, "'''"),
			Pair(58, "':'"),
			Pair(59, "';'"),
			Pair(63, "'?'"),

			Pair(126, "'~'"),
			Pair(96, "'`'"),
			Pair(124, "'|'"),
			Pair(61, "'='"),
			Pair(123, "'{'"),
			Pair(125, "'}'"),
			Pair(92, "'\\'"),
			Pair(91, "'['"),
			Pair(93, "']'"),

			Pair(60, "'<'"),
			Pair(62, "'>'")
	)
}
