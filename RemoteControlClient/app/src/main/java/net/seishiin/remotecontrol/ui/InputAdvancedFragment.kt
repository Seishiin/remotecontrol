package net.seishiin.remotecontrol.ui

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import net.seishiin.remotecontrol.R
import net.seishiin.remotecontrol.data.Constants
import net.seishiin.remotecontrol.data.TCP
import net.seishiin.remotecontrol.databinding.FragmentInputAdvancedBinding
import net.seishiin.remotecontrol.vm.StatusViewModel

class InputAdvancedFragment : Fragment() {

	private val viewModel by activityViewModels<StatusViewModel>()

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
		return inflater.inflate(R.layout.fragment_input_advanced, container, false)
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)
		val binding = FragmentInputAdvancedBinding.bind(view)

		initVM(binding)
		initUI(binding)
	}

	private fun initVM(b: FragmentInputAdvancedBinding) {
		viewModel.connectionStatus.observe(viewLifecycleOwner) { text -> b.inputAdvancedTextViewStatus.text = text }
	}

	@SuppressLint("ClickableViewAccessibility")
	private fun initUI(b: FragmentInputAdvancedBinding) {

		b.inputAdvancedButtonShowKeyboard.onKeyPressed = { key ->
			sendMessage(key) // causes infinite loop in emulator - works fine on actual devices
		}

		b.inputAdvancedViewTouchCapture.onMove = { x, y ->
			sendMessage(Constants.mouseMove(x, y))
		}

		b.inputAdvancedViewTouchCapture.onDoubleTouch = {
			sendMessage(Constants.LEFT_MOUSE_BUTTON)
		}
	}

	private fun sendMessage(msg: String) {
		lifecycleScope.launch(Dispatchers.IO) {
			viewModel.updateStatus()
			try {
				TCP.sendMsg(msg)
			} catch (e: Exception) {
				TCP.reconnect()
			}
		}
	}
}