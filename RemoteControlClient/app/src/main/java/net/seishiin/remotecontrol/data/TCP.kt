package net.seishiin.remotecontrol.data

import java.net.Socket

object TCP {
	private var client = Socket()

	fun reconnect() {
		try {
			client.close()
			client = Socket(AppPreferences.ip, AppPreferences.port)
		} catch (e: Exception) {
			// ignore
		}
	}

	fun getRemoteAddress(): String {
		return client.remoteSocketAddress.toString().trimStart('/')
	}

	fun sendMsg(msg: String?) {
		client.outputStream.write(msg?.toByteArray())
	}

	fun isConnectionAlive(): Boolean {
		try {
			client.outputStream.write("".toByteArray())
		} catch (e: Exception) {
			return false
		}
		return true
	}
}