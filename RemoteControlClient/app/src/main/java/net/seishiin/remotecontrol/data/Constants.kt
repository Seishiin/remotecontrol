package net.seishiin.remotecontrol.data

object Constants {
	const val LEFT_MOUSE_BUTTON = "LBUTTON"
	const val RIGHT_MOUSE_BUTTON = "RBUTTON"
	const val MIDDLE_MOUSE_BUTTON = "MBUTTON"
	const val COMMAND_SHUTDOWN = "CMD_SHUTDOWN"
	const val COMMAND_REBOOT = "CMD_REBOOT"
	const val COMMAND_TASK_MANAGER = "CMD_TASKMGR"
	const val COMMAND_DEVICE_MANAGER = "CMD_DEVICEMGR"
	const val COMMAND_SYSTEM_INFORMATION = "CMD_SYSINFO"
	const val COMMAND_SYSTEM_PROPERTIES = "CMD_SYSPROP"
	const val COMMAND_MUTE = "CMD_MUTE"
	const val COMMAND_UNMUTE = "CMD_UNMUTE"

	fun mouseMove(x: Int, y: Int): String {
		return "MOTION_${x}_${y}"
	}
}