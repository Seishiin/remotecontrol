package net.seishiin.remotecontrol.data

import android.content.Context
import android.content.SharedPreferences

object AppPreferences {
	private const val NAME = "remoteControl"
	private const val MODE = Context.MODE_PRIVATE
	private lateinit var preferences: SharedPreferences

	private val IP = Pair("ip", "192.168.1.70")
	private val PORT = Pair("port", 9999)
	private val MESSAGE = Pair("message", "")
	private val MESSAGE2 = Pair("message2", "")

	fun init(context: Context) {
		preferences = context.getSharedPreferences(NAME, MODE)
	}

	private inline fun SharedPreferences.edit(operation: (SharedPreferences.Editor) -> Unit) {
		val editor = edit()
		operation(editor)
		editor.apply()
	}

	var ip: String?
		get() = preferences.getString(IP.first, IP.second)
		set(value) = preferences.edit {
			it.putString(IP.first, value)
		}

	var port: Int
		get() = preferences.getInt(PORT.first, PORT.second)
		set(value) = preferences.edit {
			it.putInt(PORT.first, value)
		}

	var message: String?
		get() = preferences.getString(MESSAGE.first, MESSAGE.second)
		set(value) = preferences.edit {
			it.putString(MESSAGE.first, value)
		}

	var message2: String?
		get() = preferences.getString(MESSAGE2.first, MESSAGE2.second)
		set(value) = preferences.edit {
			it.putString(MESSAGE2.first, value)
		}
}