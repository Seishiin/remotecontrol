package net.seishiin.remotecontrol.vm

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import net.seishiin.remotecontrol.data.AppPreferences
import net.seishiin.remotecontrol.data.TCP

class StatusViewModel : ViewModel() {

	private val _buttonText = MutableLiveData<String>()
	val buttonText: LiveData<String> = _buttonText

	private val _button2Text = MutableLiveData<String>()
	val button2Text: LiveData<String> = _button2Text

	private val _connectionStatus = MutableLiveData<String>()
	val connectionStatus: LiveData<String> = _connectionStatus

	fun updateButtons() {
		_buttonText.postValue(AppPreferences.message)
		_button2Text.postValue(AppPreferences.message2)
	}

	fun updateStatus() {
		if (TCP.isConnectionAlive())
			_connectionStatus.postValue("Connected to ${TCP.getRemoteAddress()}")
		else
			_connectionStatus.postValue("Not connected")
	}
}